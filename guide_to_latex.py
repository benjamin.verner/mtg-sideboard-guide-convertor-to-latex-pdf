#!/usr/bin/env python3

import sys
from parse_input import *

def main(file):
    try:
        input_file = open(file, "r")
    except:
        raise_exception("ERROR: File unreadable (either it's not a text file, or permission is not granted)")
    
    # extract meta information from input file's first line
    colors, deck_name, format = parse_header(input_file.readline())
    output_file_name = remove_invalid_path_chars(f"{deck_name}_{format}")

    if is_existing_file(f"{output_file_name}.tex"):
        raise_exception("File with this name already exists, please remove it from the script directory")

    # generate latex's meta part (everything except the document section)
    output_file = open(f"{output_file_name}.tex", "w+")
    generate_generic_content(output_file, deck_name, format, colors)

    end_of_file = False
    current_matchup = []

    # remove first empty line
    input_file.readline()

    # while cycle to loop through all the matchups in the sideboard guide and parse them one by one
    while not end_of_file:
        line = input_file.readline()

        if len(line) == 0:
            end_of_file = True
        
        if (end_of_file or is_empty(line)):
            matchup_name, in_play, in_draw, in_always, out_play, out_draw, out_always, notes = convert_matchup(current_matchup)
            generate_table(output_file, matchup_name, in_play, in_draw, in_always, out_play, out_draw, out_always, notes)
            current_matchup = []
        else:
            current_matchup.append(remove_additional_spaces(line))

    # clean up and generate final .pdf file
    clean_up(input_file, output_file)
    make_pdf(output_file_name)
    return

if __name__ == "__main__":
    if are_valid_args(sys.argv):
        main(sys.argv[1])
