import subprocess
from functions import *

def parse_header(header):
    '''
    Extracts meta information from the header (first line of the file)
    '''

    header = header.split(" ")
    return header[0], header[1], header[2].replace("\n", "")

def generate_generic_content(output_file, deck_name, format, colors):
    '''
    Generates all the latex content before the document section
    '''

    content = open("generic_content.txt", "r")
    for line in content:
        if "\\fancyhead[L]" in line:
            output_file.write(icon_images(colors))

        elif "\\fancyhead[C]" in line:
            output_file.write(line.replace("deck_name", deck_name))

        elif "\\fancyhead[R]" in line:
            output_file.write(line.replace("format", format))

        else:
            output_file.write(line)

def convert_matchup(matchup):
    '''
    Extracts information from one matchup sideboarding guide
    '''

    matchup_name = matchup[0]
    matchup = matchup[1:]

    in_play = []
    in_draw = []
    in_always = []
    out_play = []
    out_draw = []
    out_always = []
    notes = []

    met_play = False
    met_draw = False
    met_notes = False

    for line in matchup:
        if line.startswith("-"):
            if not met_play and not met_draw:
                out_always.append(line)
            elif met_draw:
                out_draw.append(line)
            else:
                out_play.append(line)

        elif line.startswith("+"):
            if not met_play and not met_draw:
                in_always.append(line)
            elif met_draw:
                in_draw.append(line)
            else:
                in_play.append(line)

        elif line == "play:":
            met_play = True
        elif line == "draw:":
            met_draw = True
        elif line == "notes:":
            met_notes = True

        elif met_notes:
            notes.append(line)

    return matchup_name, in_play, in_draw, in_always, out_play, out_draw, out_always, notes

def generate_table(output_file, matchup_name, in_play, in_draw, in_always, out_play, out_draw, out_always, notes):
    '''
    Generates latex table for a given matchup
    '''

    output_file.write("\\begin{tabular}{|p{3cm}|p{3cm}|}\n"
                    + "\\hline" + "\n"
                    + "\\multicolumn{2}{|c|}{\\textbf{" + remove_invalid_latex_chars(matchup_name) + "}} \\\\" + "\n"
                    + "\\hline" + "\n"
                    + "OUT&IN \\\\" + "\n"
                    + "\\hline" + "\n")

    for line in range(max(len(in_play) + len(in_draw) + len(in_always), len(out_play) + len(out_draw) + len(out_always))):
        if line < len(in_play):
            in_column = "\\cellcolor{play}" + remove_invalid_latex_chars(in_play[line])
        elif line - len(in_play) < len(in_draw):
            in_column = "\\cellcolor{draw}" + remove_invalid_latex_chars(in_draw[line - len(in_play)])
        elif line - len(in_play) - len(in_draw) < len(in_always):
            in_column = remove_invalid_latex_chars(in_always[line - len(in_play) - len(in_draw)])
        else:
            in_column = ""

        if line < len(out_play):
            out_column = "\\cellcolor{play}" + remove_invalid_latex_chars(out_play[line])
        elif line - len(out_play) < len(out_draw):
            out_column = "\\cellcolor{draw}" + remove_invalid_latex_chars(out_draw[line - len(out_play)])
        elif line - len(out_play) - len(out_draw) < len(out_always):
            out_column = remove_invalid_latex_chars(out_always[line - len(out_play) - len(out_draw)])
        else:
            out_column = ""

        output_file.write(out_column + "&" + in_column + "\\\\\n")

    output_file.write("\\hline\n")

    if len(notes) != 0:
        output_file.write("\\multicolumn{2}{|p{6cm}|}{")
        output_file.write("\\begin{itemize}\n")

        for note in notes:
            output_file.write("\\item[] " + remove_invalid_latex_chars(note) + "\n")

        output_file.write("\\end{itemize}\n")
        output_file.write("} \\\\\n")

    output_file.write("\\hline\n")
    output_file.write("\\end{tabular}\n\n")
    return

def clean_up(input_file, output_file):
    '''
    Final changes & closing the files
    '''

    output_file.write("\\end{center}\n\n")
    output_file.write("\\end{document}")
    input_file.close()
    output_file.close()

def make_pdf(output_file_name):
    '''
    Converts generated latex output into a .pdf file
    '''

    subprocess.run(['pdflatex', '-interaction=nonstopmode', f"{output_file_name}.tex"], stdout = subprocess.DEVNULL)
    os.remove(f"{output_file_name}.aux")
    os.remove(f"{output_file_name}.log")
    os.remove(f"{output_file_name}.tex")