import os

def raise_exception(message):
    '''
    Prints the given message and ends the program with exit code 1
    '''

    print(message)
    exit(1)

def is_valid_file(file):
    '''
    Checks whether the given input file exists and is a file
    '''

    if not os.path.isfile(file):
        raise_exception("ERROR: Input file not found")
    else:
        return True

def are_valid_args(args):
    '''
    Checks whether the arguments given to the scripts are correct
    '''

    if (len(args) == 2 and is_valid_file(args[1])):
        return True
    else:
        raise_exception("ERROR: Wrong arguments given. This script takes exactly one argument as the path to the input file")

def remove_invalid_path_chars(string):
    '''
    Converts string into a path (removes bad chars)
    '''

    string = "".join(x for x in string if x.isalnum() or x == " " or x == "_")
    string.replace(" ", "_")
    return string

def remove_invalid_latex_chars(string):
    '''
    Removes chars that can result in commands being passed through the note string
    '''

    valid_chars = [' ', '_', '-', "'", ',', '.', '+', '/']
    return "".join(x for x in string if x.isalnum() or x in valid_chars)

def is_existing_file(string):
    '''
    Checks whether the given file already exists
    '''

    return os.path.exists(f"./{string}")

def is_empty(line):
    '''
    Checks whether the line contains only spaces and tabs
    '''

    return line.replace(" ", "").replace("\t", "") == "\n"

def icon_images(colors):
    '''
    Generates latex string converting color pips into given images in the header
    '''

    string = "\\fancyhead[L]{"
    for color in colors:
        string += "\\includegraphics[scale=0.05]{" + color + "}"
    string += "}\n"
    return string

def remove_additional_spaces(line):
    '''
    Removes spaces and tabs at the beginning and the end of a line
    '''

    while line.startswith(" ") or line.startswith("\t"):
        line = line[1:]
    while line.endswith(" ") or line.endswith("\t") or line.endswith("\n"):
        line = line[:len(line) - 1]
    return line