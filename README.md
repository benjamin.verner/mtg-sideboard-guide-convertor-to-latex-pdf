# MTG Sideboard Guide Convertor to LaTex PDF

Script for converting text sideboard guides for MTG decks to nice looking pdf

## Usage

Let's say you have a sideboard guide that you want to print and you don't want to spend all day trying to make it look nice. Then this script is exactly for you. To be fair, I made it for exactly this purpose, so the needed file format as well as the output graphical design might not be convenient for everyone.

### Sideboard Guide Format

First line should include colors the deck is using, deck name and deck format, separated by spaces. For example:

```RW Burn Modern```

A newline should follow. Then, separated by newlines, there should be guides for individual matchups. 

Guide format is the following:

- first line should include name of the opposing deck
- after that, all cards going out/in should follow, each on a different line (including numbers, so for example -4 Lava Spike)
- space for notes follows afterwards. First line should read 'notes:', after that, each new line contains one note

Example:

```
UR MURKTIDE
-3 Skullcrack
+3 Path to Exile
notes:
the most problematic card is murktide regent
possible to bring in a couple of deflecting palms
```

Syntax for highlighting cards coming in on the play/draw only is also supported. In that case, two cases should be listed, first one starting with line 'play:', second one with line 'draw:'.

Example:

```
BURN
play:
-1 Eidolon of the Great Revel
-4 Skewer the Critics
+2 Deflecting Palm
+3 Path to Exile
draw:
-4 Eidolon of the Great Revel
-1 Skewer the Critics
+2 Deflecting Palm
+3 Path to Exile
```

### Images

I do not own rights to any of the color pip images, so please get your own and place it in ./images subdirectory named W.pdf etc.

### Running the script

The script has an executable shebang, so it can be run without calling the python environment. This works on linux, otherwise, you can add python3 before the script name.

It accepts exactly one argument, which is the input text file. It generates a pdf file in the script repository.

#### Command:

```./guide_to_latex.py ./sideboard_guide.txt```

#### Output:

Pdf file in the script directory.

### Result

Using the examples, this is what output pdf looks like (there is a lot of space left, given there are only two matchups listed):

![output](doc_images/output.png)
